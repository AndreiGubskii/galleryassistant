﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {
    public static MainMenuManager instance;

    [SerializeField] private GameObject panel = null;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        panel.GetComponent<EventsHandler>().OnPointerClickEvent += ClickOnPanel;
    }

    private void ClickOnPanel() {
        HidePanel();
    }

    public void ShowPanel() {
        panel.SetActive(true);
    }

    public void HidePanel() {
        panel.SetActive(false);
    }

    public bool MenuIsActive() {
        return panel.activeSelf;
    }

    private void LoadLoginScene() {
        SceneManager.LoadScene("login");
    }

    public void Logout() {
        Scaner.instance.StopCamera(LoadLoginScene);
    }

    public void AppQuit() {
        Scaner.instance.StopCamera(Application.Quit);
    }

    public void Settings() {
        MainManager.instance.OpenPanel(Enums.Panels.SETTINGS);
    }

    public void Search()
    {
        MainManager.instance.OpenPanel(Enums.Panels.SEARCH);
    }

    public void QrReader() {
        Scaner.instance.ClickStart();
        MainManager.instance.OpenPanel(Enums.Panels.QR_READER);
    }
}
