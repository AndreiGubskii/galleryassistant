﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmWindow : MonoBehaviour {

    public delegate void ConfirmWindowDelegate();

    public event ConfirmWindowDelegate ConfirmEvent;
    public event ConfirmWindowDelegate RejectEvent;
    [SerializeField] private Text message = null;
    [SerializeField] private Button confirmBtn = null;
    [SerializeField] private Button rejectBtn = null;

    private void OnEnable()
    {
        confirmBtn.onClick.AddListener(SubmitClickListener);
        rejectBtn.onClick.AddListener(RejectClickListener);
    }

    private void OnDisable()
    {
        confirmBtn.onClick.RemoveListener(SubmitClickListener);
        rejectBtn.onClick.RemoveListener(RejectClickListener);
    }

    private void RejectClickListener() {
        RejectEvent?.Invoke();
    }

    private void SubmitClickListener()
    {
        ConfirmEvent?.Invoke();
    }

    public void SetMessage(string m)
    {
        message.text = m;
    }
}
