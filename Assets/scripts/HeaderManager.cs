﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HeaderManager : MonoBehaviour {
    public static HeaderManager instance;

    [SerializeField] private Text info = null;
    public string Info {
        set { info.text = value; }
    }

    [SerializeField] private Button menuBtn = null;

    void Awake () {
	    instance = this;
	}

    void Start() {
        menuBtn.onClick.AddListener(MenuClickListener);
        UpdateInfo(DataManager.instance.UserName);
    }

    private void OnDestroy() {
        menuBtn.onClick.RemoveListener(MenuClickListener);
    }

    private void MenuClickListener() {
        if (MainMenuManager.instance.MenuIsActive()) {
            MainMenuManager.instance.HidePanel();
        }
        else {
            MainMenuManager.instance.ShowPanel();
        }
        
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)){

            switch (MainManager.instance.ActivePanel()) {
                case Enums.Panels.QR_READER:
                    MainMenuManager.instance.AppQuit();
                    break;
                case Enums.Panels.RESULT:
                    MainMenuManager.instance.QrReader();
                    break;
                default:
                    MainMenuManager.instance.AppQuit();
                    break;
            }
            
        }
    }

    public void UpdateInfo(string userName) {
        Info = userName;
    }

    public void UpdateInfo()
    {
        Info = DataManager.instance.UserName;
    }
}
