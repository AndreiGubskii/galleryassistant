﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StockItem : MonoBehaviour
{
    [SerializeField] private Text part = null;
    [SerializeField] private Text availability = null;

    public string Part
    {
        get => part.text;
        set => part.text = value;
    }

    public string Availability
    {
        get => availability.text;
        set => availability.text = value;
    }
}
