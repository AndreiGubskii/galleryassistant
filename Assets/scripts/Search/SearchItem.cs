﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SearchItem : MonoBehaviour, IPointerClickHandler {

    [SerializeField] private Text артикул = null;
    public string Артикул {
        get => артикул.text;
        set => артикул.text = value;
    }

    [SerializeField] private Text тон = null;
    public string Тон {
        get => тон.text;
        set => тон.text = value;
    }
    
    [SerializeField] private Text количество = null;
    public string Количество {
        get => количество.text;
        set => количество.text = value;
    }

    public void OnPointerClick(PointerEventData eventData) {
        DataManager.instance.SearchResult = Артикул;
        MainManager.instance.OpenPanel(Enums.Panels.RESULT);
    }
}
