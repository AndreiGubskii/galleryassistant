﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StockController : MonoBehaviour
{
    [SerializeField] private Text Header = null;
    [SerializeField] private Text Delivery = null;
    [SerializeField] private Text Icon = null;
    [SerializeField] private GameObject itemObj = null;
    [SerializeField] private Transform itemsPanel = null;
    private UIAccordionElement element;
    private UIAccordionElement AccordionElement {
        get => element ?? (element = GetComponent<UIAccordionElement>());
    }

    public string HeaderValue
    {
        get => Header.text;
        set => Header.text = value;
    }

    public string DeliveryValue {
        get => Delivery.text;
        set => Delivery.text = value;
    }
    public GameObject ItemObj => itemObj;
    public Transform ItemsPanel => itemsPanel;

    private void Update() {
        ChangeIcon();
    }

    private void ChangeIcon() {
        
        if (AccordionElement != null)
        {
            if (AccordionElement.isOn)
            {
                Icon.text = "-";
            }
            else
            {
                Icon.text = "+";
            }
        }
    }

    public void Open(bool state) {
        StartCoroutine(OpenIEnumerator(state));
    }

    IEnumerator OpenIEnumerator(bool state) {
        yield return new WaitForEndOfFrame();
        if (AccordionElement != null)
        {
            AccordionElement.isOn = state;
        }
    }
}
