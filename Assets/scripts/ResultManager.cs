﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

public class ResultManager : MonoBehaviour {

    [SerializeField] private GameObject accordion = null;
    [SerializeField] private GameObject infoRemnantsObj = null; 
    [SerializeField] private ScrollRect scrollRect = null;
    [SerializeField] private Button viewMoreBtn = null;
    [SerializeField] private Text vendor = null;
    [SerializeField] private GameObject headers = null;
    [SerializeField] private Text noRemnantsMessage = null;

    private List<GameObject> remnantsList = new List<GameObject>();
    private void OnEnable() {
        vendor.text = "";
        noRemnantsMessage.gameObject.SetActive(false);
        headers.SetActive(true);
        if (DataManager.instance != null) {
            if (!string.IsNullOrEmpty(DataManager.instance.QrResult)) {
                Uri uriResult;
                bool isUrl = Uri.TryCreate(DataManager.instance.QrResult, UriKind.Absolute, out uriResult)
                              && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (isUrl) {
                    //Если в qr зашита ссылка
                    ParseQRData(HttpUtility.UrlDecode(new UriBuilder(DataManager.instance.QrResult).Path));
                }
                else {
                    vendor.text = DataManager.instance.QrResult;
                }
                
            }
        }
        viewMoreBtn.onClick.AddListener(ViewMoreClickListener);
    }

    private void OnDisable() {
        if (DataManager.instance != null) {
            DataManager.instance.QrResult = null;
            DataManager.instance.SearchResult = null;
        }
        ClearRemnantsList();
        viewMoreBtn.onClick.RemoveListener(ViewMoreClickListener);
    }

    
    private void ViewMoreClickListener() {
        string url = "";
        if (!string.IsNullOrEmpty(DataManager.instance.QrResult)) {
            url = DataManager.instance.QrResult.Trim() + "?utm_source=sdk";
        }
        
        Application.OpenURL(url);
        
    }

    private void ParseQRData(string value) {
        //http://www.gallery.kg/kg/p2/kgopt1/ГОУ00919069
        
        var match = new Regex(@"^\/([\S]*)\/([\S]*)\/([\S]*)\/([\S\s]*)\/?$").Match(value.Trim());
        var match_catalog = new Regex(@"p\/([\S\s]*)\/?$").Match(value.Trim());
        if (match.Success){
            string code = match.Groups[4].ToString();
            string store = match.Groups[3].ToString();
            
            StartCoroutine(GetLeftoversIEnumerator(code, store));
        }
        else if(match_catalog.Success){
            //*** КОСТЫЛЬ ****
            //Если в ссылку не зашит магазин то по умолчанию ставлю ОЦ
            //Такое Встречается в каталогах для оптовиков
            string code = match_catalog.Groups[1].ToString();
            StartCoroutine(GetLeftoversIEnumerator(code, "kgb"));
        }
        else {
            ErrorMessage("Неверный QR-code \nДля более подробной информации нажмите на кнопку \"Подробно\".");
        }

    }


    //Получить остатки
    private IEnumerator GetLeftoversIEnumerator(string code,string store)
    {
        //Остатки магазина
        //string url = string.Format("https://www.gallery.kg/api/assistant/остатки-магазина?магазин={0}&номенклатура={1}&userid={2}", store,code,DataManager.instance.UserID);

        //Остатки по складам
        string url = $"https://www.gallery.kg/api/assistant/остатки?магазин={store}&номенклатура={code}";
        
        WWWForm form = new WWWForm();
        form.AddField("userid", DataManager.instance.UserID);
        UnityWebRequest www = UnityWebRequest.Post(url,form);
        www.timeout = 0;
        LoaderUI.instance.ShowLoader();

        yield return www.SendWebRequest();

        LoaderUI.instance.HideLoaderPanel();
        if (www.isNetworkError){
            MessageManager.instance.StartWaitWindow("Проверьте подключение к сети и повторите попытку.");
        }
        else{
            try{
                //Берем остатки с сервера

                //*** Остатки магазина ***
                //ОстаткиНоменклатуры r = JsonUtility.FromJson<ОстаткиНоменклатуры>(www.downloadHandler.text);
                //CreatRemnantsList(r);
                
                //Остатки по складам
                Debug.Log(www.downloadHandler.text);
                ОстаткиНоменклатурыПоСкладам r = JsonUtility.FromJson<ОстаткиНоменклатурыПоСкладам>(www.downloadHandler.text);
                if (r.КодОтвета == "0") {
                    CreatRemnantsListByStock(r);
                }
                else {
                    vendor.text = code;
                    NoRemainsMessage();
                }
                
            }
            catch (Exception e){
                //Артикул не найден
                Debug.Log("error "+ e.Message);
                ErrorMessage(e.Message);
            }
        }
    }

    private void ClearRemnantsList() {
        remnantsList.ForEach(Destroy);
    }

    //Остатки магазина
    private void CreatRemnantsList(ОстаткиНоменклатуры r) {
        vendor.text = r.Артикул;
        if (r.Остатки.Length > 0) {
            remnantsList = new List<GameObject>();
            foreach (Остатки remnantse in r.Остатки.Where(m => m.СвободныйОстаток > 0))
            {
                GameObject obj = Instantiate(infoRemnantsObj, scrollRect.content);

                InfoRemnants infoRemnants = obj.GetComponent<InfoRemnants>();
                infoRemnants.Remnants = remnantse.СвободныйОстаток;
                if (string.IsNullOrEmpty(remnantse.Характеристика)) {
                    infoRemnants.Part = "-";
                }
                else {
                    infoRemnants.Part = remnantse.Характеристика;
                }
                
                remnantsList.Add(obj);
            }
        }
        else {
            NoRemainsMessage();
        }
        
    }

    //Остатки по складам
    private void CreatRemnantsListByStock(ОстаткиНоменклатурыПоСкладам r)
    {
        vendor.text = r.Артикул;
        if (r.Склады.Length > 0)
        {
            remnantsList = new List<GameObject>();
            for (int i = 0; i<r.Склады.Length; i++) {
                ОстаткиПоСкладам склад = r.Склады[i];
                GameObject obj = Instantiate(accordion, scrollRect.content);

                StockController stock = obj.GetComponent<AccordionController>()._StockController;
                stock.HeaderValue = (склад.Наименование.Length > 25 ? склад.Наименование.Substring(0, 25) : склад.Наименование) + " ["+склад.Остатки.Sum(m=>m.СвободныйОстаток) + "] ";
                stock.DeliveryValue = $"срочная: {r.Склады[i].КоличествоДнейСрочнаяДоставка} | несрочная: {r.Склады[i].КоличествоДнейНеСрочнаяДоставка}";
                foreach (var остатки in склад.Остатки) {
                    GameObject itemObj = Instantiate(stock.ItemObj, stock.ItemsPanel);
                    StockItem item = itemObj.GetComponent<StockItem>();

                    if (string.IsNullOrEmpty(остатки.Характеристика))
                    {
                        item.Part = "-";
                    }
                    else
                    {
                        item.Part = остатки.Характеристика;
                    }
                    
                    item.Availability = остатки.СвободныйОстаток.ToString();
                }
                remnantsList.Add(obj);
                //Первый элемент открыт по умолчанию
                if (i == 0){
                    stock.Open(true);
                }
            }
        }
        else
        {
            NoRemainsMessage();
        }

    }

    private void NoRemainsMessage() {
        noRemnantsMessage.text = "В данном магазине, запрашиваемого артикула не найдено.\nДля более подробной информации нажмите на кнопку \"Подробно\".";
        noRemnantsMessage.gameObject.SetActive(true);
        headers.SetActive(false);
    }

    private void ErrorMessage(string e)
    {
        noRemnantsMessage.text = $"Произошла ошибка: {e}";
        noRemnantsMessage.gameObject.SetActive(true);
        headers.SetActive(false);
    }
}
