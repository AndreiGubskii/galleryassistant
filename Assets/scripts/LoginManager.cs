﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{

    [SerializeField] private InputField login = null;

    [SerializeField] private InputField password = null;

    [SerializeField] private Button loginBtn = null;

    void Start() {
        DataManager.instance.UserID = null;

        if (PlayerPrefs.HasKey("login")){
            login.text = PlayerPrefs.GetString("login");
        }

        loginBtn.onClick.AddListener(LoginListener);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void LoginListener()
    {
        if (login.text != string.Empty && password.text != string.Empty )
        {
            StartCoroutine(LoginIEnumerator());
        }
        else
        {
            MessageManager.instance.StartWaitWindow("Одно или несколько полей заполнены не верно");
        }
    }

    private IEnumerator LoginIEnumerator()
    {

        WWWForm form = new WWWForm();
        form.AddField("логин", login.text);
        form.AddField("пароль", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://www.gallery.kg/api/assistant/вход", form);
        LoaderUI.instance.ShowLoader();
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
            LoaderUI.instance.HideLoaderPanel();
            MessageManager.instance.StartWaitWindow("Проверьте подключение к сети и повторите попытку.");
        }
        else
        {

            try
            {
                Пользователль пользователь = JsonUtility.FromJson<Пользователль>(www.downloadHandler.text);
                LoaderUI.instance.HideLoaderPanel();
                if (пользователь.КодОтвета == "0") {
                    DataManager.instance.UserName = пользователь.Имя;
                    DataManager.instance.UserID = пользователь.Код;
                    PlayerPrefs.SetString("login", login.text);
                    SceneManager.LoadScene("qr_reader");
                }
                else {
                    MessageManager.instance.StartWaitWindow("Неверный логин или пароль.");
                }
            }
            catch (Exception e){
                Debug.Log("error " + e.Message);
                LoaderUI.instance.HideLoaderPanel();
                MessageManager.instance.StartWaitWindow($"Произошёл сбой: {e.Message}");
            }
        }

    }
}
